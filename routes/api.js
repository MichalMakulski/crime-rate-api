const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();

const app = require('../app');
const config = require('../data/config');

router.use('/:category', prefilterRequests);
router.use('/:category', enableCORS);

router.get('/:category', (req, res, next) => {
  const categoryFile = path.resolve(__dirname, '../data', req.params.category + '.json');
  const {year, voivodship} = req.query;

  fs.readFile(categoryFile, 'utf8', (err, data) => {

    if (err) {
      res.status(500).send('Something went wrong...');
      return;
    }

    const stats = JSON.parse(data);

    if (year && voivodship) {
      res.json(stats[year][voivodship]);
      return;
    }

    if (year) {
      res.json(stats[year]);
      return;
    }

    if (voivodship) {
     handleVoivodshipReq(res, stats, voivodship);
     return;
    }

    res.json(stats);
  });
});

function handleVoivodshipReq(res, stats, voivodship) {
  const voivodshipData = {};

  for (let year in stats) {
    voivodshipData[year] = stats[year][voivodship];
  }

  res.json(voivodshipData);
}

function prefilterRequests(req, res, next) {
  const {categories, voivodships} = config;
  const category = req.params.category;
  const {year, voivodship} = req.query;

  if (category && !categories.includes(category)) {
    res.json({error: 'No such category'});
    return;
  }

  if (voivodship && !voivodships.includes(voivodship)) {
    res.json({error: 'No such voivodship'});
    return;
  }

  if (year && (parseInt(year) < 2000 || parseInt(year) > 2012)) {
    res.json({error: 'Please choose year between 2000 and 2012'});
    return;
  }

  next();
}

function enableCORS(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  next();
}

module.exports = router;