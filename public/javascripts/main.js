;(function() {
  'use strict';

  var form = document.getElementById('api-form');
  var helpBtn = document.getElementById('help-toggle');
  
  form.addEventListener('submit', function(e) {
    e.preventDefault();
    var queryInput = document.querySelector('[name="query"]');
    var query = queryInput && queryInput.value.toLowerCase();

    get('/api/' + query);
  }, false);
  
  helpBtn.addEventListener('click', function(e) {
    e.preventDefault();
    document.body.classList.toggle('help--visible');
  }, false);

  function get(url) {
    var xhr = new XMLHttpRequest();
    var responseContainer = document.querySelector('.response-container');

    xhr.addEventListener('load', function(e) {
      var response = e.target.response;
      responseContainer.innerHTML = '<pre>' + response + '</span>';
    });
    
    xhr.addEventListener('error', function(err) {
      responseContainer.innerHTML = '<span style="color: #ff0000">Wystąpił problem z Twoim zapytaniem.</span>';
    });

    xhr.open('GET', url);
    xhr.send();
  }

})();