var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Statystyka przestępoczści w Polsce w latach 2000 - 2012' });
});

module.exports = router;
